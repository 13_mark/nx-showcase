import { Component } from '@angular/core';

@Component({
  selector: 'humanaize-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'connect';
  private xc?: string;

  constructor(private readonly httpClient: any) {}

  test(): number { return 2}

  private x() {}
}
